function Task2 () {
	var number = document.getElementById('number').value;
	function reverse(num){
  		var reversed = "";    
  		for (var i = num.length - 1; i >= 0; i--){        
    		reversed += num[i];
  		}    
  		return reversed;
	}
	jsConsole.writeLine(+reverse(number));
}