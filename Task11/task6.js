function Task6 () {
    //{4, 1, 1, 4, 2, 3, 4, 4, 1, 2, 4, 9, 3} -> 4 (5 раз)
    var array = document.getElementById('array').value;
    array = array.split(',');
    for(var i = 0;i<array.length;i++){
        array[i] = +array[i];
    }
    var max = array[0];
    var count = 0;
    for (var i = 0; i < array.length; i++) {
        var newCount = 0;
        for (var j = 0; j < array.length; j++) {
            if (array[i] == array[j]) {
                newCount++;
            }
        }
        if (newCount > count) {
            count = newCount;
            max = array[i];
        }
    }
    jsConsole.writeLine("Element:" + max + "<br>" +  'Count:' + count);
}