// 4. Напишите скрипт, который находит максимальную 
// возрастающую последовательность в массиве.
// Например, 	[3, 2, 3, 4, 2, 2, 4] -> [2, 3, 4].
function Task4 () {
	var array = document.getElementById('array').value;
	var array = array.split(',')
	var count = 1;
	var end = 0;
	var newCount = 1;
	for (var i = 0; i < array.length; i++) {
		array[i] = +array[i];
	}
	for (var i = 0; i < array.length; i++) {
		if (array[i] < array[i + 1]) {
			newCount++;
		}else{
			newCount = 1;
		}
		if (newCount > count) {
			count = newCount;
			end = i + 1;
		}
	}
	jsConsole.write("Max Sequence:");
	for (i = end + 1 - count; i < end + 1; i++) {
		jsConsole.write(" " + array[i] + " ");
	}
	jsConsole.write('<br>');
}