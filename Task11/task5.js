function Task5() {
	var inputArray = document.getElementById('array').value;
	inputArray = inputArray.split(',');
	jsConsole.writeLine('Sorted array ' + selectionSort(inputArray));
	function selectionSort(array) {
		var n = array.length;
		for(var i = 0;i<array.length;i++){
			array[i] = +array[i];
		}
		for (var i = 0; i < n - 1; i++) {
			var min = i;
			for (var j = i + 1; j < n; j++) {
				if (array[j] < array[min]) {
					min = j;
				}
			}
			var t = array[min];
			array[min] = array[i];
			array[i] = t;
		}
		return array;
	}
}