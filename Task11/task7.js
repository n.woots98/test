function Task7() {
	var inputArray = document.getElementById('array').value;
	var inputItem = document.getElementById('item').value;
	inputArray = inputArray.split(',');
	if (binarySearch(inputArray, inputItem) == -1) {
		jsConsole.writeLine("Array doesn't have this item");
	} else {
		jsConsole.writeLine('Position ' + binarySearch(inputArray, inputItem));
	}

	function binarySearch(array, item) // item - искомый элемент, array - упорядоченный массив, в котором ищем.
	{
		function choose(a, b) {
			return a - b;
		}
		array.sort(choose);
		var i = 0;
		var j = array.length;
		var k;
		while (i < j) {
			k = Math.floor((i + j) / 2);
			if (item <= array[k]) {
				j = k;
			} else {
				i = k + 1;
			}
		}
		if (array[i] === item) {
			return i;
		} else {
			return -1;
		}
	}
}