var dragMaster = (function() {
    var dragObject;
    var mouseDownAt;
    var currentDropTarget;

    function mouseDown(event) {
        mouseDownAt = {
            x: event.pageX,
            y: event.pageY,
            element: this
        };
        addDocumentEventHandlers();
        return false;
    }

    function mouseMove(event) {
        if (mouseDownAt) {
            var elem = mouseDownAt.element;
            dragObject = elem.dragObject;
            var mouseOffset = getMouseOffset(elem, mouseDownAt.x, mouseDownAt.y);
            mouseDownAt = null;
            dragObject.onDragStart(mouseOffset);
        }
        dragObject.onDragMove(event.pageX, event.pageY);
        var newTarget = getCurrentTarget(event);
        if (currentDropTarget != newTarget) {
            if (currentDropTarget) {
                currentDropTarget.onLeave();
            }
            if (newTarget) {
                newTarget.onEnter();
            }
            currentDropTarget = newTarget;
        }
        return false;
    }

    function mouseUp() {
        if (!dragObject) {
            mouseDownAt = null;
        } else {
            if (currentDropTarget) {
                currentDropTarget.accept(dragObject);
                dragObject.onDragSuccess(currentDropTarget);
            } else {
                dragObject.onDragFail();
            }
            dragObject = null;
        }
        removeDocumentEventHandlers();
    }
    function getMouseOffset(target, x, y) {
        var docPos = getOffset(target);
        return {
            x: x - docPos.left,
            y: y - docPos.top
        };
    }
    function getCurrentTarget(event) {
        var x = event.pageX,
            y = event.pageY;
        dragObject.hide();
        var elem = document.elementFromPoint(x, y);
        dragObject.show();
        return elem.dropTarget;
    }

    function addDocumentEventHandlers() {
        document.onmousemove = mouseMove;
        document.onmouseup = mouseUp;
        document.ondragstart = document.body.onselectstart = function() {
            return false;
        }
    }
    function removeDocumentEventHandlers() {
        document.onmousemove = document.onmouseup = document.ondragstart = document.body.onselectstart = null;
    }
    return {
        makeDraggable: function(element) {
            element.onmousedown = mouseDown;
        }
    }
}())
function DragObject(element) {
    element.dragObject = this;
    dragMaster.makeDraggable(element);
    var rememberPosition;
    var mouseOffset;
    this.onDragStart = function(offset) {
        var style = element.style
        rememberPosition = {
            top: style.top,
            left: style.left,
            position: style.position
        };
        style.position = 'absolute';
        mouseOffset = offset;
    }
    this.hide = function() {
        element.style.display = 'none';
    }
    this.show = function() {
        element.style.display = '';
    }
    this.onDragMove = function(x, y) {
        element.style.top = y - mouseOffset.y + 'px';
        element.style.left = x - mouseOffset.x + 'px';
    }
    this.onDragSuccess = function(dropTarget) {}
    this.onDragFail = function() {
        var style = element.style;
        style.top = event.pageX;
        style.left = event.pageY;
        style.position = event.position;
    }
    this.toString = function() {
        return element.id
    };
}

function DropTarget(element) {
    element.dropTarget = this;
    this.canAccept = function(dragObject) {
        return true;
    }
    this.accept = function(dragObject) {
        this.onLeave();
        dragObject.hide();
    }
    this.onLeave = function() {
        element.className = '';
        element.src = 'close.jpg';
    }
    this.onEnter = function() {
        element.className = 'uponMe';
        element.src = 'open.png';
    }
    this.toString = function() {
        return element.id;
    }
}

function getOffset(elem) {
    if (elem.getBoundingClientRect) {
        return getOffsetRect(elem);
    } else {
        return getOffsetSum(elem);
    }
}

function getOffsetRect(elem) {
    var box = elem.getBoundingClientRect()
    var body = document.body;
    var docElem = document.documentElement;
    var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop;
    var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;
    var clientTop = docElem.clientTop || body.clientTop || 0;
    var clientLeft = docElem.clientLeft || body.clientLeft || 0;
    var top = box.top + scrollTop - clientTop;
    var left = box.left + scrollLeft - clientLeft;
    return {
        top: Math.round(top),
        left: Math.round(left)
    };
}

function getOffsetSum(elem) {
    var top = 0,
        left = 0;
    while (elem) {
        top = top + parseInt(elem.offsetTop);
        left = left + parseInt(elem.offsetLeft);
        elem = elem.offsetParent;
    }
    return {
        top: top,
        left: left
    };
}