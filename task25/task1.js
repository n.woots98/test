function man () {
	var canvas = document.getElementById('canv');
	if (canvas.getContext){
	    var ctx = canvas.getContext('2d');
	    //mainCircle
	    ctx.fillStyle = '#20B2AA';
	    ctx.strokeStyle = '#008080';
	    ctx.beginPath();
	    ctx.arc(250,250,100,0,Math.PI*2,true);
	    ctx.fill();
	    ctx.stroke();

	    //rot
	    ctx.beginPath();
	    ctx.strokeStyle = '#008080';
	    ctx.translate(0,0);
	    ctx.scale(50 / 15, 1);
	    ctx.arc(75, 300, 10, 0, Math.PI * 2, true);
	    ctx.restore();
	    ctx.stroke();

	    //nos
	    ctx.beginPath();
	    ctx.strokeStyle = '#008080';
	    ctx.moveTo(70,270);
	    ctx.lineTo(80,230);
	    ctx.moveTo(70,270);
	    ctx.lineTo(77,270);
	    ctx.stroke();

	    //eyeLeft
	    ctx.beginPath();
	    ctx.strokeStyle = '#008080';
	    ctx.translate(0, 0);
	    ctx.scale(10 / 15, 1);
	    ctx.arc(100, 200, 8, 0, Math.PI * 2, true);
	    ctx.restore();
	    ctx.stroke();

	    //eyeLeft
	    ctx.beginPath();
	    ctx.strokeStyle = '#008080';
	    ctx.translate(0, 0);
	    ctx.scale(10 / 10, 1);
	    ctx.arc(130, 200, 8, 0, Math.PI * 2, true);
	    ctx.restore();
	    ctx.stroke();

	    //eyeAppleLeft
	    ctx.beginPath();
	    ctx.fillStyle = 'black';
	    ctx.translate(0, 0);
	    ctx.scale(10 / 10, 4);
	    ctx.arc(96, 50, 2, 0, Math.PI * 2, true);
	    ctx.restore();
	    ctx.fill();

	    //eyeAppleRight
	    ctx.beginPath();
	    ctx.fillStyle = 'black';
	    ctx.translate(0, 0);
	    ctx.scale(10 / 10, 1);
	    ctx.arc(126, 50, 2, 0, Math.PI * 2, true);
	    ctx.restore();
	    ctx.fill();

	    //hat
	    ctx.beginPath();
	    ctx.fillStyle = 'blue';
	    ctx.strokeStyle = 'black';
	    ctx.translate(0, 0);
	    ctx.scale(100 / 15, 1);
	    ctx.arc(17, 40, 8, 0, Math.PI * 2, true);
	    ctx.restore();
	    ctx.fill();
	    ctx.stroke();

	    ctx.beginPath();
		ctx.moveTo(17, 36);
		ctx.arc(17, 36, 5.5, 0, Math.PI, false);
		ctx.stroke();

		ctx.beginPath();
		//ctx.moveTo(12, 36);
		ctx.rect(11.5,36,11,-31);
		ctx.stroke();
		ctx.fill();

		ctx.beginPath();
	    ctx.fillStyle = 'blue';
	    ctx.translate(0, 0);
	    ctx.scale(10 / 10, 1);
	    ctx.arc(17, 5, 5.5, 0, Math.PI * 2, true);
	    ctx.restore();
	    ctx.fill();
	    ctx.stroke();
	}
}
function bicycle () {
	var canvas = document.getElementById('canv');
	if (canvas.getContext){
	    var ctx = canvas.getContext('2d');

	    ctx.beginPath();
	    ctx.fillStyle = '#20B2AA';
	    ctx.strokeStyle = 'red';
	    ctx.scale(10 / 25, 1);
	    ctx.moveTo(85,95);
	    ctx.lineTo(100,70);
	    ctx.lineTo(110,70);
	    ctx.lineTo(120,70);
	    ctx.lineTo(105,95);
	    ctx.lineTo(85,95);
	    ctx.stroke();

	    ctx.beginPath();
	    ctx.arc(105,95,4,0,Math.PI*2,true);
	    //pedal2
	    ctx.moveTo(106,99);
	    ctx.lineTo(110,105);
	    //pedal2
	    ctx.moveTo(102,93);
	    ctx.lineTo(98,87);
	    ctx.stroke();

	    ctx.moveTo(105,95);
	    ctx.lineTo(95,60);
	    ctx.moveTo(90,60);
	    ctx.lineTo(100,60);
	    ctx.stroke();

	    //left tyre
	    ctx.beginPath();
	    ctx.moveTo(90,95);
	    ctx.arc(85,95,11,0,Math.PI*2,true);
	    ctx.stroke();

	    //right tyre
	    ctx.beginPath();
	    ctx.arc(125,95,11,0,Math.PI*2,true);
	    ctx.closePath();
	    ctx.stroke();

	    ctx.beginPath();
	    ctx.moveTo(125,95);
	    ctx.lineTo(117,50);
	    ctx.moveTo(117,50);
	    ctx.lineTo(110,60);
	    ctx.moveTo(117,50);
	    ctx.lineTo(120,40);
	    ctx.stroke();
	}
}
function house () {
	var canvas = document.getElementById('canv');
	if (canvas.getContext){
	    var ctx = canvas.getContext('2d');

	    ctx.beginPath();
	    ctx.fillStyle = '#8B4513';
	    ctx.strokeStyle = 'black';
		ctx.rect(50,250,80,100);
		ctx.lineTo(90,180);
		ctx.lineTo(130,250);
		ctx.closePath();
		ctx.fill();
		ctx.stroke();

		//truba
		ctx.beginPath();
		ctx.rect(105,235,7,-45);
		ctx.stroke();
		ctx.fill();
		ctx.beginPath();
	    ctx.fillStyle = '#8B4513';
	    ctx.translate(0, 0);
	    ctx.scale(10 / 10, 1);
	    ctx.arc(108.6, 190, 3.7, 0, Math.PI * 2, true);
	    ctx.restore();
	    ctx.fill();
	    ctx.stroke();

	    //Windows
	    ctx.beginPath();
	    ctx.fillStyle = 'black';
	    //left
	    ctx.rect(53,270,15,-14);
	    ctx.rect(70,270,15,-14);
	    ctx.rect(53,287,15,-14);
	    ctx.rect(70,287,15,-14);
	    //rightTop
	    ctx.rect(95,270,15,-14);
	    ctx.rect(112,270,15,-14);
	    ctx.rect(95,287,15,-14);
	    ctx.rect(112,287,15,-14);
	    //rightBottom
	    ctx.rect(95,320,15,-14);
	    ctx.rect(112,320,15,-14);
	    ctx.rect(95,337,15,-14);
	    ctx.rect(112,337,15,-14);
	    ctx.fill();
	    ctx.stroke();
	    //door
	    ctx.beginPath();
	    ctx.moveTo(60,350);
	    ctx.lineTo(60,310);
	    ctx.moveTo(72,350);
	    ctx.lineTo(72,298);
	    ctx.moveTo(84,350);
	    ctx.lineTo(84,310);
	    ctx.arc(72,310,12,0, Math.PI,true);
	    ctx.moveTo(70,335);
	    ctx.arc(68,335,2,0, Math.PI * 2,true);
	    ctx.moveTo(78,335);
	    ctx.arc(76,335,2,0, Math.PI * 2,true);
	    ctx.stroke();
	}
}
man();
bicycle();
house();