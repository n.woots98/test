function create() {
	var divs = document.getElementsByClassName('divs'),
	    delta = 72,
	    x = 0,
		y = 0,
		angle = 0,
		radius = 150,
		centerX = 400,
		centerY = 450;
	for (var i = 0; i < divs.length; i++) {
		divs[i].style.position = 'absolute';
		divs[i].style.display = 'block';
		x = (centerX + radius * Math.cos(2 * Math.PI * i / 5));
        y = (centerY + radius * Math.sin(2 * Math.PI * i / 5));
		divs[i].style.left = x + 'px';
		divs[i].style.top = y + 'px';
		angle += delta;
	}
}

function animateDivs(t) {
	var divs = document.getElementsByClassName('divs');
	var x = 0,
		y = 0,
		delta = 72,
		centerX = 400,
        centerY = 450,
        angle = 0,
        radius = 150;
	for (var i = 0; i < divs.length; i++) {
        var div = divs[i];
        x = centerX + radius * Math.cos( (180 * i + t) * Math.PI / 720);
        y = centerY + radius * Math.sin( (180 * i + t) * Math.PI / 720);
 	 	div.style.left = x + 'px';
        div.style.top = y + 'px';
        angle+=delta;
    }
    window.requestAnimationFrame(animateDivs);
}