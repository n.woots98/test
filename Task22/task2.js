function Task22_2 () {
	var people = [
        { name: "Шапокляк", age: 55 },
        { name: "Чебурашка", age: 17 },
        { name: "Крыска-Лариска", age: 18 },
        { name: "Крокодильчик", age: 26 },
        { name: "Турист- завтрак крокодильчика", age: 32 },
    ];
    var divPeople = document.getElementById("list-item");
    var template = divPeople.innerHTML;
    var peopleList = generateHtmlList(people, template);
    divPeople.innerHTML = '<ul><li>' + peopleList + '</li></ul>';

    function generateHtmlList (arr,temp) {
    	var nameReg = /{name}/gi;
    	var ageReg = /{age}/gi;
    	var str = '';
    	for (var i = 0; i<arr.length;i++) {
    		str += '<li>' + temp.replace(nameReg,arr[i].name).replace(ageReg,arr[i].age) + '</li>';
    	}
    	return '<ul>' + str + '</ul>';
    }



}