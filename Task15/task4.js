function fib (n) {
	var a = 1;//first elem
	var b = 1;//second elem
	for(var i = 3; i <= n; i++){
		var sum = a + b;
		a = b;
		b = sum;
	}
	return b;
}