function Task14_3 () {
	var text = document.getElementById('text').value;
	var str = document.getElementById('str').value;
	var reg = new RegExp(str, 'igm');
	if (text.match(reg) == null ) {
		jsConsole.writeLine('There are 0 occurances of the word ' + str + '.');
	}else {
		jsConsole.writeLine('There are ' + text.match(reg).length + ' occurances of the word ' + str + '.');
	}
}