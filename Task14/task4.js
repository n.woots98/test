function Task14_4() {
  var text = document.getElementById('text').value;
  jsConsole.writeLine(text);
  var span = document.getElementsByTagName('span')[0];
    for (var i = 0; i < span.childNodes.length; i++) {
      if (span.childNodes[i].nodeType != 1) continue;


      if (span.childNodes[i].tagName == 'UPCASE') {
        span.childNodes[i].textContent = span.childNodes[i].textContent.toUpperCase();
      }else {
        if (span.childNodes[i].tagName == 'LOWCASE') {
          span.childNodes[i].textContent = span.childNodes[i].textContent.toLowerCase();
        }
        if (span.childNodes[i].tagName == 'MIXCASE') {
                var mixt=span.childNodes[i].textContent;
                var func = toMix(span.childNodes[i].textContent);
            span.childNodes[i].textContent = func; 
        } 
      }
    }
}
function toMix(mixt){ 
    var str = mixt; 
    var newstr=""; 
    for (var i = 0; i < str.length; i++){ 
      var c = 0; 
      c = Math.floor(Math.random()*11); 
      if (c < 6){ 
        newstr=newstr+str[i].toUpperCase(); 
      }else{
        newstr=newstr+str[i].toLowerCase();
      } 
    }
  return newstr;
} 
