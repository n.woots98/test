function gen(){
    var abcdArray = [];
    abcdArray[0] = Math.floor(Math.random()*(10-1)+1);
    for(var i = 1;i < 4;i++){
        var rand = Math.floor(Math.random()*10);
        if(abcdArray.indexOf(rand) == -1){
            abcdArray.push(rand);
        }else{
        	i--;
        }
    }
    return abcdArray;
}

var genNumber = gen();
var str = genNumber.join('');
var koll = 0;
console.log(str);

function check () {
	var inputNumber = document.getElementById('number').value;
	var div = document.getElementById('div');
	var span = document.getElementById('span');
	var arr = inputNumber.split('');
	var sheep = 0;
	var ram = 0;
	
	if (inputNumber.length != 4 || inputNumber[0] == 0) {
		div.innerHTML += 'check your input <br>';
		return 0; 
	}

	for (var i = 0; i < arr.length; i++) {
		arr[i] = +arr[i]
	}

	if (inputNumber != str) {
		for (var i = 0; i < 4; i++) {
			if (arr[i] == genNumber[i]) {
				ram++;
			}
			for (var j = 0; j < 4; j++) {
				if (arr[i] == genNumber[j]) {
					sheep++;
				}
			}
		}
		div.innerHTML += 'You have: ' + ram + ' ram(s) ' + ' and ' + (sheep-ram) + ' sheep(s)' + '<br>';
		koll++;
	}else {
		span.hidden = '';
	}
}
function sub () {
	var i = 0;
	var name = document.getElementById('name').value;
	var div2 = document.getElementById('results');
	
	if (!localStorage.getItem('game')) { 
		localStorage.setItem('game', JSON.stringify([{name:name, count:koll + " moves"}])); 
	}else{ 
		var game =JSON.parse(localStorage.getItem('game')); 
		game.push({name:name, count:koll + " moves"}) 
		localStorage.setItem('game',JSON.stringify( game)); 
	} 
	var game =JSON.parse(localStorage.getItem('game')); 
	for (var i = 0; i <game.length; i++){
		div2.innerHTML += game[i].name + ": " + game[i].count + '<br>'; 
	}
}