var obj1 = {
	a: 1,
	b: 2
};
var obj2 = {
	a: 'qwe',
	b: 'asd'
};
function clone(obj) {
    if (null == obj || "object" != typeof obj){
    	return obj;
    }
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)){
        	copy[attr] = obj[attr];
        }
    }
    return copy;
}
var objClone1 = clone(obj1);
var objClone2 = clone(obj2);
function Task18_3 () {
	for (var key in objClone1) {
	jsConsole.writeLine("Obj1: "+objClone1[key]);
	}
	for (var key in objClone2) {
		jsConsole.writeLine("Obj2: "+objClone2[key]);
	}
}