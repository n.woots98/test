function Arr(arr) {
	this.arr = arr;
}
Arr.prototype.remove = function(elem){
	for (var i = 0; i < this.arr.length; i++) {
		if (this.arr[i] === elem) {
			this.arr.splice(i, 1);
		}
	}
	return this.arr;
};

var solution = new Arr([1, 2, 1, 4, 1, 3, 4, 1, 111, 3, 2, 1, "1"]);

function Task18_2 () {
	jsConsole.writeLine(solution.remove(1));
}