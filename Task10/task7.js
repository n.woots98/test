// 1) Дан целочисленный вектор А(n). Подсчитать 
// сколько раз встречается в этом векторе минимальное по величине число.
var array = [9, 3, 6, 2, 5, 1, 3, 7, 1, 2, 4];
var min = Infinity;
var count = 0;
for(var i = 0; i < array.length; i++){
	if (array[i] <= min) {
		min = array[i];
	}
}
for(var i = 0; i < array.length; i++){
	if (array[i] == min) {
		count++;
	}
}
document.writeln('Min = ' + min + ';<br>' + 'Count = ' + count+';');
