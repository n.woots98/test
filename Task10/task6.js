// Task 3 
// Используя циклы нарисуйте в браузере с помощью пробелов (&nbsp) и звездочек (*): 
// · Прямоугольник 
// · Прямоугольный треугольник  
// · Равносторонний треугольник 
// · Ромб.
function rectangle () {
	var a = 10;
	var b = 3;
	//creating line with a stars
	var line = '*'.repeat(a);
	for(var i = 0; i < b; i++){
		document.write(line);
		document.write('<br>');
	}
}
function triangle90 () {
	for (var a = "*"; a.length <= 5; a += "*"){
		document.write(a);
		document.write('<br>');
	}
	
}
function triangle () {
	var lines = 5;
    var space = 6;
    var star = 1; 

    for (var height = 0; height < lines; height++) {
        for (var wSpace = 0; wSpace < space; wSpace++) {
            document.writeln("&nbsp\n");
        }
        for (var wStar = 0; wStar < star; wStar++) {
            document.write("*");
        }
        space -= 1;
        star += 2;
        document.writeln("<br>");
    }
}
function romb () {
	var i = 5;
  	var lines = i;
  	var star = 1;
  	var space = 6;

	while (i-- > 0) {
  		for (var wSpace = 0; wSpace < space; wSpace++) {
            document.writeln("&nbsp\n");
        }
        for (var wStar = 0; wStar < star; wStar++) {
            document.write("*");
        }
        space -= 1;
        star += 2;
        document.writeln("<br>");
	}
	i = 0;
	while (++i < lines) {
  		for (var wSpace = 0; wSpace < space; wSpace++) {
            document.writeln("&nbsp\n");
        }
        for (var wStar = 0; wStar < star; wStar++) {
            document.write("*");
        }
        space += 1;
        star -= 2;
        lines = 7;
        document.writeln("<br>");
	}
	
}
rectangle();
document.write('-------------------------<br>');
triangle90();
document.write('-------------------------<br>');
triangle();
document.write('-------------------------<br>');
romb();