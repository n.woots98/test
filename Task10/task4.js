// Дано два числа A и B где (A<B).  
// Выведите на экран суму всех чисел расположенных в числовом промежутке от А до В.  
// Выведите на экран все нечетные значения, расположенные в числовом промежутке от А до В.
var a = +prompt("Input a","");
var b = +prompt("Input b","");
var sum = 0;
if (a >= b) {
	document.writeln('Error: a >= b!');
}else {
	a++;
	document.writeln('Все нечетные значения:<br>');
	while(a < b){
    	sum += a;
       	if (a % 2 != 0) {
        	document.writeln(a);
        	document.writeln('<br>');
       	}
       	a++;
    }
	document.writeln("Sum = " + sum);
}