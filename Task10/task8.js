// Task 2 
// Требуется: Создать двумерный массив элементов размерностью 5х5 
// и заполнить его произвольными целочисленными значениями.
// По главной диагонали все числа со знаком (-) заменить на 0, а числа со знаком (+) на число 1.
var matrix = [
  [1, 2, 3, -2, 5],
  [4, -5, 6, 1, 9],
  [7, 8, 9, -6, -3],
  [-4, 6, 1, 3, 5],
  [7, 5, -3, 5, -7]
];
document.writeln('Input matrix<br>')
for(var i = 0; i < matrix.length; i++){
	for(var j = 0; j < matrix.length; j++){
		document.writeln(matrix[i][j]);
	}
	document.write('<br>');
}
document.write('-----------------------------');
document.write('<br>');
document.write('Changed matrix<br>');
for(var i = 0; i < matrix.length; i++){
	for(var j = 0; j < matrix.length; j++){
		if (i == j) {
			if (matrix[i][j] < 0) {
				matrix[i][j] = 0;
			}else {
				matrix[i][j] = 1;
			}
		}
		document.writeln(matrix[i][j]);
	}
	document.write('<br>');
}