function Task19_3 () {
    
    var persons = [
      { firstname: "Natalya", lastname: "Osipenko", age: 61 },
      { firstname: "Kristina", lastname: "Osipenko", age:23 },
      { firstname: "Artem", lastname: "Korhov", age: 25 },
      { firstname: "Artem", lastname: "Seredinskiy", age: 20 },
      { firstname: "Artem", lastname: "Artsiomenka", age: 20 },
      { firstname: "Sergey", lastname: "Osipenko", age: 20 },
      { firstname: "Vinni", lastname: "Puh", age: 15 }
   ];
   var groupedByAge = groupPeople (persons, "age");
   var groupedByLastName = groupPeople(persons, "lastname");
   var groupedByFirstName = groupPeople (persons, "firstname");
   outputOfArray(groupedByAge);
   outputOfArray(groupedByLastName);
   outputOfArray(groupedByFirstName);
   function groupPeople(obj, key) {
      var newArray = [];
      var array = obj.reduce(function(arr, elem) {
         arr.push(elem[key]);
         return arr;
      }, []);
      point: for (var i = 0; i < array.length; i++) {
         var elemOfArray = array[i];
         for (var j = 0; j < newArray.length; j++) {
            if (newArray[j] == elemOfArray) {
               continue point;
            }
         }
         newArray.push(elemOfArray);
      }
      return newArray;
   };

   function outputOfArray(array) {
      for (var i = 0; i < array.length; i++) {
         jsConsole.writeLine(array[i]);
      }
   }
}