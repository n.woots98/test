function Task19_2 () {
	var persons = [
	    { firstName : "Gosho", lastName: "Petrov", age: 32 },
	    { firstName : "Bay", lastName: "Ivan", age: 81 },
	    { firstName : "Prig", lastName: "Skok", age: 19 },
	    { firstName : "asd", lastName: "hjk", age: 20 },
	    { firstName : "zxc", lastName: "nmn", age: 44 }
    ];
    var min = persons[0].age;
    for (var i = 0; i < persons.length; i++) {
    	if (persons[i].age < min) {
    		min = persons[i];
    	}
    }
    jsConsole.writeLine(min.firstName + ' ' + min.lastName);
}