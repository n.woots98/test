function Task20_3 () {
	var str = '<p>Please visit <a href="http://academy.telerik. com">our site</a> to choose a training course. Also visit <a href="www.devbg.org">our forum</a> to discuss the courses.</p>';
	var startReg = /<a href/gi;
	var endReg = /<\/a>/gi;
	var strelkaReg = />/gi;
	var reg = /(<([^>]+)>)/gi;

	str = str.replace(startReg,'[URL ');
	str = str.replace(endReg,'[/URL]');
	str = str.replace(reg,'');
	str = str.replace(strelkaReg,']');
	jsConsole.writeLine(str);
}