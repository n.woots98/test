function Task13_1 (param1,param2) {
	var text = document.getElementById('textArea').value;
	var word = document.getElementById('word').value;
	regWord = '\\b' + word + '\\b';
	var reg = new RegExp(regWord, 'igm');
	if (text.match(reg) == null ) {
		jsConsole.writeLine('There are 0 occurances of the word ' + word + '.');
	}else {
		jsConsole.writeLine('There are ' + text.match(reg).length + ' occurances of the word ' + word + '.');
	}
}