function get() {
	$.getJSON("01.data.json", function(data) {
		var items = [];
		for (var i = 0; i <= 6; i++) {
			$.each(data[i], function(key, val) {
				items.push("<ul><li>" + key + " : " + val + "</li></ul>");
			});
		}
		$('#get').html('');
		$('#get').append(items.join(''));
	});
}

function post() {
	var firstName = document.getElementById('firstName').value;
	var lastName = document.getElementById('lastName').value;
	var age = document.getElementById('age').value;
	var postDiv = document.getElementById('post');
	var objJSON = {};
	var promise = new Promise(function(resolve, reject) {
		var req = new XMLHttpRequest();
		req.open('POST', "http://localhost/01.data.json", true);
		req.setRequestHeader('Content-Type', 'application/json')
		req.onload = function() {
			if (this.readyState != 4) {
				var error = new Error(this.statusText);
				error.code = this.status;
				reject(error);
			} else {
				resolve(this.response);
			}
		};
		req.onerror = function() {
			reject(new Error("Network Error"));
		};
		var obj = {
			"firstName": firstName,
			"lastName": lastName,
			"age": age
		};
		objJSON = JSON.stringify(obj);
		req.send(objJSON);
	});
	promise.then(response => postDiv.innerHTML += JSON.stringify(objJSON), error => postDiv.innerHTML += JSON.stringify(error));
}